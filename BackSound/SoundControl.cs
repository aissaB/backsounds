﻿using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using System.IO;
using NAudio.Wave;
namespace BackSound
{   
    public class SoundControl:Component
    {
        //properties
        private Image icon;
        private string name;
        private int volume;
        private bool IsActive;
        private bool hasSound;
        //Controls
        private Control Parent;
        private Panel bgPanel;
        private RoundPictureBox rpbx;
        private TrackBar soundVol;
        private CheckBox cbxSound;
        private FrmMain mainForm;
        //Sounds
        private WaveOutEvent outputDevice;
        private AudioFileReader audioFile;
        private bool ClickedToStop;
        private string pathToSoundFile;

        

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="PannelParent">the pannel containing the pannels</param>
        /// <param name="main"> the main form</param>
        /// <param name="pathToFolder"> the path to the folder containing the files</param>
        /// <param name="posX"> the position x</param>
        /// <param name="posY"> the position Y</param>
        public SoundControl(Control PannelParent,FrmMain main,string pathToFolder,int posX,int posY){
            mainForm = main;
            this.Parent = PannelParent;
            outputDevice = new WaveOutEvent();
            outputDevice.PlaybackStopped += OnPlayBackStopped;
            ClickedToStop = false;
            Create(posX,posY);
            string[] theFilesOfTheFolder = Directory.GetFiles(pathToFolder);
            foreach (string file in theFilesOfTheFolder)
            {
                if (IsItAPicture(file))
                {
                    Icon = Image.FromFile(file);
                }
                if (Path.GetExtension(file) == ".txt")
                {
                    string[] allLines = File.ReadAllLines(file);
                    this.Name = allLines[0];
                    this.Volume = Convert.ToInt32(allLines[1]);
                    this.IsActive = (allLines[2] == "0") ? false : true;
                }
                if ((IsItASound(file)))
                {
                    pathToSoundFile = file;
                    InitialiseMusic();
                   this.hasSound = true;
                }
            }
        }
        /// <summary>
        /// Change the volume of the music readers
        /// </summary>
        /// <param name="volume"></param>
        /// <returns></returns>
        private float ChangeVolumeOnWaveInput(int volume)
        {
            float diviseur=volume;
            return (diviseur / 100f);
        }
        /// <summary>
        /// Getter setters
        /// </summary>
        public Image Icon {
            get {
                return icon;
            }
            set
            {
                icon = value;
                rpbx.Image = value;
                mainForm.ChangePicture(value);
            }
            
           
        }
        public string Name {
            get
            {
                return name;
            }
            set
            {
                name = value;
                cbxSound.Text = value;
                mainForm.ChangeTitle(value);
            }
        }
        public int Volume {
            get
            {
                return volume;
            }
            set
            {
                if (value>=100 || value<=0)
                {
                    if (value>=100)
                    {
                        volume = 100;
                    }
                    else
                    {
                        volume = 0;
                    }
                    soundVol.Value = volume;
                }
                else
                {
                    volume = value;
                    soundVol.Value = value;
                }


            }
        }
        /// <summary>
        /// Create the controls
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void Create(int x,int y) {
            //Panel
            Panel p = new Panel();
            p.Width = 305;
            p.Height = 73;
            p.BackColor = Color.Gray;
            p.Location = new Point(x, y);
            bgPanel = p;
            //picturebox
            RoundPictureBox pbx = new RoundPictureBox();
            pbx.Width = 50;
            pbx.Height = 50;
            pbx.Location = new Point(250,10);
            pbx.SizeMode = PictureBoxSizeMode.StretchImage;
            rpbx = pbx;
            //trackBar
            TrackBar soundVolume = new TrackBar();
            soundVolume.Location= new Point(0,45);
            soundVolume.Minimum = 0;
            soundVolume.Maximum = 100;
            soundVolume.TickStyle = TickStyle.None;
            soundVolume.Width = 227;
            soundVolume.ValueChanged += (s, e) => { ChangeSoundVolume();};
            soundVol = soundVolume;
            //CheckBox
            CheckBox cbx = new CheckBox();
            cbx.Location =new Point(8, 0);
            cbx.Text = "Playing";
            cbx.Checked = false;
            cbx.CheckedChanged += (s, e) => { PlayStopSound(); } ;
            cbxSound = cbx;
           
            //assign to parent
            p.Controls.Add(pbx);
            p.Controls.Add(soundVolume);
            p.Controls.Add(cbx);
            Parent.Controls.Add(p);
        }
        /// <summary>
        /// Check if the file in the given path is a playable sound
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool IsItASound(string file)
        {
            return (Path.GetExtension(file).ToLower() == ".mp3" || Path.GetExtension(file).ToLower() == ".wav") ? true : false;
        }
        /// <summary>
        /// Check if the file in the given path is picture
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private bool IsItAPicture(string file)
        {
            return (Path.GetExtension(file).ToLower() == ".jpg" || Path.GetExtension(file).ToLower() == ".png" || Path.GetExtension(file).ToLower() == ".gif" ||Path.GetExtension(file).ToLower() == ".jpeg") ? true : false;
        }
        /// <summary>
        /// Function given to the onValueChanged of the trackbar
        /// </summary>
        private void ChangeSoundVolume()
        {
            this.volume = soundVol.Value;
            outputDevice.Volume = ChangeVolumeOnWaveInput(this.volume);
        }
        /// <summary>
        /// Changement when the checkbox is changed stop the sound if it's a user request or loop the song
        /// </summary>
        private void PlayStopSound() {
            if (hasSound)
            {
                setSoundAndMusic();
                if (IsActive)
                {
                    ClickedToStop = true;
                    IsActive = false;
                    outputDevice.Stop();
                }
                else
                {
                    InitialiseMusic();
                    outputDevice.Play();
                    IsActive = true;
                }
                
            }
        }
        /// <summary>
        /// Function for the random selection on startup
        /// </summary>
        public void setSoundAndMusic() {
            mainForm.ChangePicture(this.icon);
            mainForm.ChangeTitle(this.name);
        }
        /// <summary>
        /// Initialisation of the music
        /// </summary>
        private void InitialiseMusic() {
            outputDevice = new WaveOutEvent();
            audioFile = new AudioFileReader(pathToSoundFile);
            outputDevice.Init(audioFile);
            outputDevice.PlaybackStopped += OnPlayBackStopped;
            outputDevice.Volume = ChangeVolumeOnWaveInput(this.volume);
        }
        /// <summary>
        /// Actions to take when the music come to an end
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPlayBackStopped(object sender, StoppedEventArgs e)
        {
            outputDevice.Dispose();
            outputDevice = null;
            audioFile.Dispose();
            audioFile = null;
            InitialiseMusic();
            if (!ClickedToStop)
            {
                outputDevice.Play();
            }
            else
            {
                ClickedToStop = !ClickedToStop;
            }
            
        }

        public bool HasSound() {
            return hasSound;
        }
        public void RemoveFromPannel()
        {
            this.Parent.Controls.Remove(this.bgPanel);
        }
    }
}
