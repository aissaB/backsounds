﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
namespace BackSound
{
    public partial class FrmMain : Form
    {

        
        public FrmMain()
        {
            InitializeComponent();
            Installation.CheckSoundsPresence();
            CreateTheSounds(Directory.GetDirectories(Installation.GetPathToFiles()));
        }
        #region Borderless windows moving
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void FrmMain_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        #endregion
        private void CreateTheSounds(string[] path) {
            int x = 10;
            int xcpt = 0;
            int y = 15;
            if (path.Length<=0)
            {
                Installation.DeleteSoundsFolder();
                Installation.CheckSoundsPresence();
            }
            foreach (var sound in path)
            {

                SoundControl newSound = new SoundControl(soundControlPannel, this, sound, x, y);
                if (newSound.HasSound())
                {
                    xcpt++;
                    if (xcpt < 4)
                    {
                        x += 310;
                    }
                    else
                    {
                        xcpt = 0;
                        x = 10;
                        y += 80;
                    }
                }
                else
                {
                    newSound.RemoveFromPannel();
                }
            }
        }
        private void ClearTheSounds() {
            soundControlPannel.Controls.Clear();
        }
        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void FrmMain_Load(object sender, EventArgs e)
        {
            lblCurrentSound.Parent = pbxMainImage;
            lblCurrentSound.BackColor = Color.Transparent;
        }
        public void ChangePicture(Image newPicture) {
            pbxMainImage.Image = newPicture;
        }
        public void ChangeTitle(string title) {
            lblCurrentSound.Text = title;
        }
        private void BtnReduce_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
