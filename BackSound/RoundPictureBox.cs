﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace BackSound
{
    class RoundPictureBox:PictureBox
    {
        public RoundPictureBox()
        {

        }
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            using (var gp = new GraphicsPath())
            {
                gp.AddEllipse(0, 0, this.Width - 8, this.Height - 3);
                this.Region = new Region(gp);
            }
            
        }
    }
}
