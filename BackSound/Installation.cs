﻿using System;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackSound
{
    public static class Installation
    {
        /// <summary>
        /// Get the path to the folder containing the sound
        /// </summary>
        /// <returns></returns>
        public static string GetPathToFiles() {
            return System.Environment.CurrentDirectory + "\\" + "sounds";
        }
        /// <summary>
        /// Check if the Sounds exist and if not creates them
        /// </summary>
        /// <returns></returns>
        public static bool CheckSoundsPresence() {
            if (Directory.Exists(GetPathToFiles()))
            {
                return true;
            }
            else
            {
                CreateFoldersAndFiles();
                return false;
            }
        }
        /// <summary>
        /// Create the folder and send the content of the folders
        /// </summary>
        private static void CreateFoldersAndFiles() {
            Directory.CreateDirectory(GetPathToFiles());
            CreateContentOfFolder("Fire", Properties.Resources.Fire2, Properties.Resources.feu_de_bois_chaleureux);
            CreateContentOfFolder("Forest", Properties.Resources.rainforest_ambience_GlorySunz_1938133500, Properties.Resources.in_the_forest);
            CreateContentOfFolder("Frogs", Properties.Resources.Frogs_Lisa_Redfern_1150052170, Properties.Resources.green_frog_1543328044JuC);
            CreateContentOfFolder("Lake", Properties.Resources.Whales_Spouting_Out_Water_SoundBible_com_966355632, Properties.Resources.blue_water_background_2_1425096008vKC);
            CreateContentOfFolder("Night Crickets", Properties.Resources.Night_Sounds___Crickets_Lisa_Redfern_591005346, Properties.Resources.green_grasshopper_11292184739ag0);
            CreateContentOfFolder("mountain stream", Properties.Resources.Babbling_Brook_SoundBible_com_17660315, Properties.Resources.riviere_de_montagne_1457210539nEv);
            CreateContentOfFolder("Summer day", Properties.Resources.Sunny_Day_SoundBible_com_2064222612, Properties.Resources.sunshine_background_1473794522BJz);
        }
        /// <summary>
        /// Create the stream of the mp3
        /// </summary>
        /// <param name="name"></param>
        /// <param name="array"></param>
        /// <param name="picture"></param>
        private static void CreateContentOfFolder(string name,byte[] array,Image picture) {
            Stream x = GetStreamOfSounds(array);
            FolderAndFilesCreation(name, picture, x);
            x.Dispose();
        }
        /// <summary>
        /// Get the stream from the byte array
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        private static MemoryStream GetStreamOfSounds(byte[] array) {
            MemoryStream stream = new MemoryStream();
            stream.Write(array, 0, array.Length);
            return stream;
        }
        /// <summary>
        /// Create the picture,mp3 and txt file needed
        /// </summary>
        /// <param name="directoryName"></param>
        /// <param name="pictureToUse"></param>
        /// <param name="mp3Origin"></param>
        private static void FolderAndFilesCreation(string directoryName,Image pictureToUse, Stream mp3Origin) {
            string path = GetPathToFiles() + "\\" + directoryName;
            Directory.CreateDirectory(path);
            pictureToUse.Save(path + "\\" + directoryName + ".jpg");
            var mp3File = File.Create(path + "\\" + directoryName + ".mp3");
            mp3Origin.Seek(0, SeekOrigin.Begin);
            mp3Origin.CopyTo(mp3File);
            mp3File.Close();
            using (StreamWriter sw = File.CreateText(path + "\\" + directoryName + ".txt"))
            {
                sw.WriteLine(directoryName);
                sw.WriteLine("50");
                sw.WriteLine("0");
                sw.WriteLine("0");
            }
        }
        public static void DeleteSoundsFolder()
        {
            Directory.Delete(GetPathToFiles());
        }

    }
}
