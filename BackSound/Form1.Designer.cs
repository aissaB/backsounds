﻿namespace BackSound
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCurrentSound = new System.Windows.Forms.Label();
            this.soundControlPannel = new System.Windows.Forms.Panel();
            this.btnQuit = new System.Windows.Forms.Button();
            this.BtnReduce = new System.Windows.Forms.Button();
            this.pbxMainImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMainImage)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCurrentSound
            // 
            this.lblCurrentSound.AutoSize = true;
            this.lblCurrentSound.Font = new System.Drawing.Font("Nasalization", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentSound.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblCurrentSound.Location = new System.Drawing.Point(12, 3);
            this.lblCurrentSound.Name = "lblCurrentSound";
            this.lblCurrentSound.Size = new System.Drawing.Size(99, 33);
            this.lblCurrentSound.TabIndex = 1;
            this.lblCurrentSound.Text = "Test";
            // 
            // soundControlPannel
            // 
            this.soundControlPannel.AutoScroll = true;
            this.soundControlPannel.BackColor = System.Drawing.SystemColors.InfoText;
            this.soundControlPannel.Location = new System.Drawing.Point(1, 508);
            this.soundControlPannel.Name = "soundControlPannel";
            this.soundControlPannel.Size = new System.Drawing.Size(1272, 183);
            this.soundControlPannel.TabIndex = 2;
            // 
            // btnQuit
            // 
            this.btnQuit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnQuit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuit.Font = new System.Drawing.Font("Minerva", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuit.Location = new System.Drawing.Point(1215, -1);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(58, 37);
            this.btnQuit.TabIndex = 3;
            this.btnQuit.Text = "X";
            this.btnQuit.UseVisualStyleBackColor = false;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // BtnReduce
            // 
            this.BtnReduce.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BtnReduce.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnReduce.Font = new System.Drawing.Font("Minerva", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnReduce.Location = new System.Drawing.Point(1151, -1);
            this.BtnReduce.Name = "BtnReduce";
            this.BtnReduce.Size = new System.Drawing.Size(58, 37);
            this.BtnReduce.TabIndex = 4;
            this.BtnReduce.Text = "—";
            this.BtnReduce.UseVisualStyleBackColor = false;
            this.BtnReduce.Click += new System.EventHandler(this.BtnReduce_Click);
            // 
            // pbxMainImage
            // 
            this.pbxMainImage.Location = new System.Drawing.Point(1, 42);
            this.pbxMainImage.Name = "pbxMainImage";
            this.pbxMainImage.Size = new System.Drawing.Size(1256, 463);
            this.pbxMainImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxMainImage.TabIndex = 0;
            this.pbxMainImage.TabStop = false;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(1269, 723);
            this.Controls.Add(this.BtnReduce);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.soundControlPannel);
            this.Controls.Add(this.lblCurrentSound);
            this.Controls.Add(this.pbxMainImage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FrmMain";
            this.Text = "frmMain";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FrmMain_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.pbxMainImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxMainImage;
        private System.Windows.Forms.Label lblCurrentSound;
        private System.Windows.Forms.Panel soundControlPannel;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Button BtnReduce;
    }
}

